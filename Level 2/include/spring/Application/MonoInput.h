#include <qaudioformat.h>
#include <qiodevice.h>
#include <qvector.h>
#include <qendian.h>
#include <iostream>

namespace Spring
{
	class MonoInput: public QIODevice
	{
		private:
			QAudioFormat *mAudioFormat;
			QVector<double> mSamples;
			qint32 mMaxAmplitude;
			double dataLength;
			int channelBytes;

		public:
			~MonoInput();

			MonoInput(double displayTime, int sampleRate);

			qint64 readData(char* data, qint64 maxlen)override;

			qint64 writeData(const char* data, qint64 len)override;

			QAudioFormat getAudioFormat();

			QVector<double> vecGetData();

	};
}