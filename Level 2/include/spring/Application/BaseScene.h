#pragma once

#include "qcustomplot.h"
#include <qpushbutton.h>
#include <qgridlayout.h>
#include <qaudioinput.h>
#include <spring\Framework\IScene.h>
#include <spring\Application\MonoInput.h>

namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT
			private slots:
				void mf_StartInput();
				void mf_StopInput();
				void mf_PlotRandom();
				void mf_CleanPlot();
				void returnClick();

		private:
			QWidget *centralWidget;
			QGridLayout *centralWidgetLayout;
			QCustomPlot *customPlot;
			QGridLayout *gridLayout;
			QPushButton *stopButton;
			QPushButton *startButton;
			QPushButton *returnButton;
			QSpacerItem *horizontalSpacer;
			QVector<double> xAxis;
			QVector<double> yAxis;
			MonoInput *mMonoInput;
			QAudioInput *mAudioInput;
			unsigned int refreshRate;
			unsigned int sampleRate;
			double displayTime;

		public:
			explicit BaseScene(const std::string &ac_szSceneName);

			void createScene()override;

			void release()override;

			void createGUI();
	};
}
