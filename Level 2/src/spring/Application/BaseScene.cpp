#include <spring\Application\BaseScene.h>

namespace Spring
{
	BaseScene::BaseScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{

	}

	void BaseScene::createScene()
	{
		std::string windowTitle = boost::any_cast<std::string>(m_TransientDataCollection.find("appNameLEdit")->second);
		refreshRate = boost::any_cast<unsigned>(m_TransientDataCollection.find("refreshRateDSBox")->second);
		sampleRate = boost::any_cast<unsigned>(m_TransientDataCollection.find("sampleRateDSBox")->second);
		displayTime = boost::any_cast<double>(m_TransientDataCollection.find("displayTimeDSBox")->second);

		m_uMainWindow->setWindowTitle(QString::fromStdString(windowTitle));
		createGUI();

		connect(startButton, SIGNAL(clicked()), this, SLOT(mf_StartInput()));
		connect(stopButton, SIGNAL(clicked()), this, SLOT(mf_StopInput()));
		connect(returnButton, SIGNAL(clicked()), this, SLOT(returnClick()));
	

		customPlot->addGraph();
		double nrSamples = sampleRate * displayTime;
		mMonoInput = new MonoInput(displayTime, sampleRate);
		for (int i = 0; i < nrSamples; i++)
		{
			xAxis.push_back((double)i * 1.0 / sampleRate);
		}
	}

	void BaseScene::release()
	{
		delete centralWidget;
	}

	void BaseScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		centralWidgetLayout = new QGridLayout(centralWidget);
		centralWidgetLayout->setSpacing(5);
		centralWidgetLayout->setContentsMargins(10, 10, 10, 10);
		centralWidgetLayout->setObjectName(QStringLiteral("centralWidgetLayout"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(5);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));

		customPlot = new QCustomPlot(centralWidget);
		customPlot->setObjectName(QStringLiteral("customPlot"));
		gridLayout->addWidget(customPlot, 0, 0, 1, 4);

		startButton = new QPushButton(centralWidget);
		startButton->setObjectName(QStringLiteral("startButton"));
		gridLayout->addWidget(startButton, 2, 1, 1, 1);

		stopButton = new QPushButton(centralWidget);
		stopButton->setObjectName(QStringLiteral("stopButton"));
		gridLayout->addWidget(stopButton, 2, 2, 1, 1);

		returnButton = new QPushButton(centralWidget);
		returnButton->setObjectName(QStringLiteral("returnButton"));
		gridLayout->addWidget(returnButton, 2, 3, 1, 1);

		startButton->setText("Start");
		stopButton->setText("Stop");
		returnButton->setText("Back");

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
		gridLayout->addItem(horizontalSpacer, 2, 0, 1, 1);

		centralWidgetLayout->addLayout(gridLayout, 0, 0, 1, 1);
		m_uMainWindow.get()->setCentralWidget(centralWidget);
	}

	void BaseScene::mf_CleanPlot()
	{
		customPlot->graph(0)->data()->clear();
		customPlot->replot();
	}

	void BaseScene::returnClick()
	{
		emit SceneChange("InitialScene");
	}

	void BaseScene::mf_PlotRandom()
	{
		yAxis = mMonoInput->vecGetData();
		xAxis.resize(yAxis.size());

		customPlot->graph(0)->setData(xAxis, yAxis);
		customPlot->graph(0)->rescaleAxes();
		customPlot->replot();
	}

	void BaseScene::mf_StartInput()
	{
		mAudioInput = new QAudioInput(mMonoInput->getAudioFormat(), this);
		mMonoInput->open(QIODevice::OpenModeFlag::WriteOnly);
		mAudioInput->setNotifyInterval(1000 / refreshRate);
		mAudioInput->start(mMonoInput);
		connect(mAudioInput, SIGNAL(notify()), this, SLOT(mf_PlotRandom()));
	}

	void BaseScene::mf_StopInput()
	{
		mMonoInput->close();
		mAudioInput->stop();
		mf_CleanPlot();
	}
}
