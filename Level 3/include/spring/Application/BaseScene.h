#pragma once

#include <qtimer.h>
#include "qcustomplot.h"
#include <qpushbutton.h>
#include <qgridlayout.h>
#include <qaudioinput.h>
#include <aquila\global.h>
#include <spring\Framework\IScene.h>
#include <aquila\transform\FftFactory.h>
#include <spring\Application\MonoInput.h>

namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT
			private slots:
				void mf_StartTimer();
				void mf_StopTimer();
				void mf_PlotRandom();
				void mf_CleanPlot();
				void returnClick();

		private:
			QWidget *centralWidget;
			QGridLayout *centralWidgetLayout;
			QCustomPlot *customPlot;
			QCustomPlot *frequencyPlot;
			QGridLayout *gridLayout;
			QPushButton *stopButton;
			QPushButton *startButton;
			QPushButton *returnButton;
			QSpacerItem *horizontalSpacer;
			QVector<double> xAxis;
			QVector<double> yAxis;
			QVector<double> amplitude;
			QVector<double> frequency;
			QTimer *timer;
			MonoInput *mMonoInput;
			QAudioInput *mAudioInput;
			unsigned int refreshRate;
			unsigned int sampleRate;
			double displayTime;
			const int FFTSize = 128;
			double frequencyRes;
			std::shared_ptr<Aquila::Fft> fft;
			std::vector<Aquila::ComplexType> spectrum;


		public:
			explicit BaseScene(const std::string &ac_szSceneName);

			void createScene()override;

			void release()override;

			void createGUI();
	};
}
