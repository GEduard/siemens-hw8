#include <spring\Application\MonoInput.h>

namespace Spring
{

	MonoInput::MonoInput(double displayTime, int sampleRate)
	{
		mAudioFormat = new QAudioFormat();
		mAudioFormat->setSampleRate(sampleRate);
		mAudioFormat->setChannelCount(1);
		mAudioFormat->setSampleSize(16);
		mAudioFormat->setSampleType(QAudioFormat::UnSignedInt);
		mAudioFormat->setByteOrder(QAudioFormat::LittleEndian);
		mAudioFormat->setCodec("audio/pcm");
		dataLength = displayTime * sampleRate;
		channelBytes = 2;
		mMaxAmplitude = 32767;
	}

	MonoInput::~MonoInput()
	{

	}

	qint64 MonoInput::readData(char * data, qint64 maxlen)
	{
		Q_UNUSED(data);
		Q_UNUSED(maxlen);
		return -1;
	}

	qint64 MonoInput::writeData(const char * data, qint64 len)
	{
		const auto *ptr = reinterpret_cast<const unsigned char *>(data);
		for (int i = 0; i < len / channelBytes; i++)
		{
			qint32 value = 0;
			value = qFromLittleEndian<qint16>(ptr);

			auto level = float(value) * (5.0 / mMaxAmplitude);
			mSamples.push_back(level);
			ptr += channelBytes;
		}

		if (mSamples.size() > dataLength)
			mSamples.remove(0, mSamples.size() - dataLength);

		return len;
	}

	QAudioFormat MonoInput::getAudioFormat()
	{
		return *mAudioFormat;
	}

	QVector<double> MonoInput::vecGetData()
	{
		return mSamples;
	}
}