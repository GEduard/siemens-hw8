#include <spring\Application\MonoInput8bit.h>

namespace Spring
{

	MonoInput8bit::MonoInput8bit(double displayTime, int sampleRate)
	{
		mAudioFormat = new QAudioFormat();
		mAudioFormat->setSampleRate(sampleRate);
		mAudioFormat->setChannelCount(1);
		mAudioFormat->setSampleSize(8);
		mAudioFormat->setSampleType(QAudioFormat::UnSignedInt);
		mAudioFormat->setByteOrder(QAudioFormat::LittleEndian);
		mAudioFormat->setCodec("audio/pcm");
		dataLength = displayTime * sampleRate;
		channelBytes = 1;
		mMaxAmplitude = 32767;
	}

	MonoInput8bit::~MonoInput8bit()
	{

	}

	qint64 MonoInput8bit::readData(char * data, qint64 maxlen)
	{
		Q_UNUSED(data);
		Q_UNUSED(maxlen);
		return -1;
	}

	qint64 MonoInput8bit::writeData(const char * data, qint64 len)
	{
		const auto *ptr = reinterpret_cast<const unsigned char *>(data);

		for (int i = 0; i < len / channelBytes; i++)
		{
			qint32 value = 0;
			value = qFromLittleEndian<qint8>(ptr);

			auto level = float(value) * (5.0 / mMaxAmplitude);
			mSamples.push_back(level);
			ptr += channelBytes;
		}

		if (mSamples.size() > dataLength)
			mSamples.remove(0, mSamples.size() - dataLength);

		return len;
	}

	QAudioFormat MonoInput8bit::getAudioFormat()
	{
		return *mAudioFormat;
	}

	QVector<double> MonoInput8bit::vecGetData()
	{
		return mSamples;
	}
}
