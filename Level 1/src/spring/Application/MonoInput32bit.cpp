#include <spring\Application\MonoInput32bit.h>

namespace Spring
{

	MonoInput32bit::MonoInput32bit(double displayTime, int sampleRate)
	{
		mAudioFormat = new QAudioFormat();
		mAudioFormat->setSampleRate(sampleRate);
		mAudioFormat->setChannelCount(1);
		mAudioFormat->setSampleSize(32);
		mAudioFormat->setSampleType(QAudioFormat::SignedInt);
		mAudioFormat->setByteOrder(QAudioFormat::LittleEndian);
		mAudioFormat->setCodec("audio/pcm");
		dataLength = displayTime * sampleRate;
		channelBytes = 4;
		mMaxAmplitude = 32767;
	}

	MonoInput32bit::~MonoInput32bit()
	{

	}

	qint64 MonoInput32bit::readData(char * data, qint64 maxlen)
	{
		Q_UNUSED(data);
		Q_UNUSED(maxlen);
		return -1;
	}

	qint64 MonoInput32bit::writeData(const char * data, qint64 len)
	{
		const auto *ptr = reinterpret_cast<const signed char *>(data);

		for (int i = 0; i < len / channelBytes; i++)
		{
			qint32 value = 0;
			value = qFromLittleEndian<qint32>(ptr);

			auto level = float(value) * (5.0 / mMaxAmplitude);
			mSamples.push_back(level);
			ptr += channelBytes;
		}

		if (mSamples.size() > dataLength)
			mSamples.remove(0, mSamples.size() - dataLength);

		return len;
	}

	QAudioFormat MonoInput32bit::getAudioFormat()
	{
		return *mAudioFormat;
	}

	QVector<double> MonoInput32bit::vecGetData()
	{
		return mSamples;
	}
}