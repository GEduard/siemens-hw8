#pragma once

#include <qtimer.h>
#include "qcustomplot.h"
#include <qpushbutton.h>
#include <qgridlayout.h>
#include <qaudioinput.h>
#include <spring\Framework\IScene.h>
#include <spring\Application\MonoInput.h>
#include <spring\Application\MonoInput8bit.h>
#include <spring\Application\MonoInput32bit.h>

namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT
			private slots:
				void mf_StartTimer();
				void mf_StopTimer();
				void mf_PlotRandom();
				void mf_CleanPlot();
				void returnClick();

		private:
			QWidget *centralWidget;
			QGridLayout *centralWidgetLayout;
			QCustomPlot *customPlot;
			QGridLayout *gridLayout;
			QPushButton *stopButton;
			QPushButton *startButton;
			QPushButton *returnButton;
			QSpacerItem *horizontalSpacer;
			QVector<double> xAxis;
			QVector<double> yAxis;
			QTimer *timer;
			MonoInput *mMonoInput;
			MonoInput8bit *mMonoInput8bit;
			MonoInput32bit *mMonoInput32bit;
			QAudioInput *mAudioInput;
			unsigned int refreshRate;
			unsigned int sampleRate;
			double displayTime;

		public:
			explicit BaseScene(const std::string &ac_szSceneName);

			void createScene()override;

			void release()override;

			void createGUI();
	};
}
